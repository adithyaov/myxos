decl
	integer status;
	integer fp;
	string word;
	string wordread;
	string filename;

	integer n;
	integer i;
	integer fp2;
	integer choice;
	integer enter;
	integer seekPos;
enddecl
integer main()
{
	enter = 1;
	while(enter == 1) do
		print("The choices are as follows");
		print("1 : Create");
		print("2 : Open");
		print("3 : Close");
		print("4 : Delete");
		print("5 : Write");
		print("6 : Seek");
		print("7 : Read");
		print("8 : Exit");
		print("9 : Find");
		print("N?");
		read (n);
	
		if(n == 1) then
			print("Name?");
			read(filename);
			status = Create(filename);
			print(status);
		endif;
		if(n == 2) then
			print("Name?");
			read(filename);
			fp = Open(filename);
			print(fp);
		endif;
		if(n == 3) then
			print("Open FD?");
			read(fp2);
			fp = Seek(fp2, 0);
			while (enter == 1) do
				status = Read(fp2, word);
				if(status == -1) then
					break;
				endif;
				print(word);
			endwhile;				
			status = Close(fp2);
			print(status);
		endif;
		if(n == 4) then
			print("Name?");
			read(filename);
			status = Delete(filename);
			print(status);
		endif;
		if(n == 5) then
			print("Open FD?");
			read(fp);
			print ("Word?");
			read(word);
			status = Write(fp,word);
			print(status);
		endif;
		if(n == 6) then

			print("Open FD?");
			read(fp);
			print("Seek pos?");
			read(seekPos);
			status = Seek(fp, seekPos);
			print(status);

		endif;
		if(n == 7) then
			print("Open FD?");
			read(fp);
			status = Read(fp, word);
			print("Word =>");
			print(word);
			print(status);
		endif;
		if(n == 8) then
			enter = 0;
		endif;
		if(n == 9) then
			print("Word?");
			read(wordread);
			print("Open FD?");
			read(fp2);
			i = 0;
			fp = Seek(fp2, 0);
			while (enter == 1) do
				status = Read(fp2, word);
				if(status == -1) then
					break;
				endif;
				if(word == wordread) then
					i = i + 1;
				endif;
			endwhile;
			print(i);
		endif;

	endwhile;
	


		
	return 0;
}
