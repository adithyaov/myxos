
fdisk
load --init ../myFiles/stage8/syscall.xsm
load --int=7 ../myFiles/stage4/int7.xsm
load --int=1 ../myFiles/stage8/int1.xsm
load --int=2 ../myFiles/stage8/int2.xsm
load --int=3 ../myFiles/stage8/int3.xsm
load --int=4 ../myFiles/stage8/int4.xsm
load --exhandler ../myFiles/stage4/exhandler.xsm
load --os ../myFiles/stage8/os_startup.xsm


spl
./spl --os ../myFiles/stage8/os_startup.spl
./spl --int=1 ../myFiles/stage8/int1.spl
./spl --int=2 ../myFiles/stage8/int2.spl
./spl --int=3 ../myFiles/stage8/int3.spl
./spl --int=4 ../myFiles/stage8/int4.spl

apl
./apl ../myFiles/stage8/syscall.apl

xfs
./xfs-interface

xsm
./xsm --timer=0

