decl
	integer isPrime(integer n);
enddecl
integer isPrime(integer n)
{
	integer i, retVal;
	i = 2;
	retVal = -1;
	if (n == 1) then
		retVal = 1;
	endif;
	if (n == 2) then
		retVal = 0;
	endif;
	while (i < n) do
		if (retVal == 0) then
			break;
		endif;
		if ((n % i) == 0) then
			retVal = 1;
			break;
		endif;
		i = i + 1;
	endwhile;
	return retVal;
}
integer main()
{
	print("Enter n ");
	integer n, retVal, i;
	read(n);
	retVal = 0;
	i = 1;
	while (i < n) do
		retVal = isPrime(i);
		if (retVal == 0) then
			print(i);
		endif;
		if (retVal == -1) then
			print(i);
		endif;
		i = i + 1;
	endwhile;
	return 0;
}
